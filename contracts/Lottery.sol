//SPDX-License-Identifier: MIT
 pragma solidity ^0.8.0;

import "@chainlink/contracts/src/v0.8/interfaces/AggregatorV3Interface.sol";
import "@chainlink/contracts/src/v0.8/interfaces/VRFCoordinatorV2Interface.sol";
import "@chainlink/contracts/src/v0.8/interfaces/LinkTokenInterface.sol";
import "@chainlink/contracts/src/v0.8/VRFConsumerBaseV2.sol";
import "@openzepplin/contracts/access/Ownable.sol";

 contract Lottery is VRFConsumerBaseV2, Ownable
 {
    address payable[] private players;
    address payable public latestWinner;
    uint256 public ticketPriceInUSD;

    AggregatorV3Interface internal priceFeed; //Chainlink price feed

    /* Chainlink random number generator stuff*/
    uint64 public s_subscriptionId;
    VRFCoordinatorV2Interface COORDINATOR;
    LinkTokenInterface public LINKTOKEN;
    uint256 public s_requestId;
    bytes32 keyHash;
    uint16 requestConfirmations = 3;
    uint32 numWords =  1;
    uint32 callbackGasLimit = 100000;

    enum LOTTERY_STATE{
        OPEN,
        CLOSED,
        CALCULATING_WINNER
    }
    LOTTERY_STATE public lottery_state;

    //https://docs.chain.link/docs/vrf-contracts/
    constructor(address _pricefeed, address _vrfCoordinator, address _linktokenContract,
    bytes32 _keyhash)
    VRFConsumerBaseV2(_vrfCoordinator)
    {
        lottery_state = LOTTERY_STATE.CLOSED;
        ticketPriceInUSD = 50;
        priceFeed = AggregatorV3Interface(_pricefeed);
        COORDINATOR = VRFCoordinatorV2Interface(_vrfCoordinator);
        LINKTOKEN = LinkTokenInterface(_linktokenContract);
        s_subscriptionId = COORDINATOR.createSubscription();
        COORDINATOR.addConsumer(s_subscriptionId, address(this)); //add the current contract as consumer
        keyHash = _keyhash;
    }

    function transferLinkToContract(uint256 amount) internal returns(bool)
    {
        return LINKTOKEN.transferFrom(tx.origin, address(this), amount);
    }

    function getAllowance(address owner) public view returns(uint256)
    {
        return LINKTOKEN.allowance(owner, address(this));
    }

    function returnContractLinkBalance() external onlyOwner returns(bool)
    {
        return LINKTOKEN.transfer(msg.sender, LINKTOKEN.balanceOf(address(this)));
    }

    function topUpSubscription(uint256 amount) internal returns(bool)
    {
        return LINKTOKEN.transferAndCall(address(COORDINATOR), amount, abi.encode(s_subscriptionId));
    }

    function cancelSubscription() external onlyOwner
    {
        COORDINATOR.cancelSubscription(s_subscriptionId, msg.sender);
    }

    function getSubscription() internal 
        returns(uint96 balance, uint64 reqCount, address owner, address[] memory consumers)
    {
        return COORDINATOR.getSubscription(s_subscriptionId);
    }

    /* The owner should make sure the token allowance to this contract is more than 1000000000000000000
    before calling startLottery since we might require to topup the subscription by 1000000000000000000*/
    function startLottery() public onlyOwner
    {
        require( lottery_state == LOTTERY_STATE.CLOSED, "Lottery state must be closed to start the lottery" );
        
        uint96 balance;
        (balance,,,) = getSubscription();

        if(balance < 1000000000000000000) //If the subscription balance is less than 1 LINK
        {
            if(!transferLinkToContract(1000000000000000000))
            {
                revert("Transfer of Link to contract failed");
            }
    
            if (!topUpSubscription(1000000000000000000))
            {
                revert("Could not tranfer link from contract to VRF Subscription");
            }
        }
        lottery_state = LOTTERY_STATE.OPEN;
    }

    function buyTicket() public payable
    {
        require(lottery_state == LOTTERY_STATE.OPEN, "Lottery is not open");
        require( getEntranceFee() <= msg.value, "Not enough ETH!"); //min 50usd ticket entry fee
        players.push(payable(msg.sender));
    }

    function getEntranceFee() public view returns(uint256)
    {
        (,int256 answer,,,) = priceFeed.latestRoundData();
        uint256 oracle_ethusd_price = uint256 (answer);
        return (ticketPriceInUSD * 10 ** (18+8))/oracle_ethusd_price;
    }

    // Assumes the subscription is funded sufficiently.
    function requestRandomWords() internal onlyOwner {
        // Will revert if subscription is not set and funded.
        s_requestId = COORDINATOR.requestRandomWords(
        keyHash,
        s_subscriptionId,
        requestConfirmations,
        callbackGasLimit,
        numWords
        );
    }

    function fulfillRandomWords(uint256, /* requestId */ uint256[] memory randomWords) internal override 
    {
        require(lottery_state == LOTTERY_STATE.CALCULATING_WINNER, "Lottery is not yet ready to declare a winner");
        lottery_state = LOTTERY_STATE.CLOSED;
        latestWinner = players[randomWords[0]%(players.length)];
        latestWinner.transfer(address(this).balance);
        /*Reset the lottery to play again*/
        players = new address payable[](0);
    }

    function endLottery() public onlyOwner/// @notice Explain to an end user what this does
    {
        lottery_state = LOTTERY_STATE.CALCULATING_WINNER;
        requestRandomWords();
    }

    /*function createorGetSubscription(uint64 subscriptionId) private onlyOwner 
    {
        try COORDINATOR.getSubscription(subscriptionId)
        {
            s_subscriptionId = subscriptionId;
        }
        catch (bytes memory reason)
        {
            s_subscriptionId = COORDINATOR.createSubscription();
        }
    }*/
 }
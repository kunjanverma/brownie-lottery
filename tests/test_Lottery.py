from brownie import Lottery, network, config
import scripts.helpers
from web3 import Web3

def getLotteryContract(deploy_account):
    if not Lottery:
        return Lottery.deploy(config["networks"]["mainnet-fork"]["eth-usd-price-feed"],
                              {"from":deploy_account})
    else:
        return Lottery[-1]
    
def test_ticket_price():
    deploy_account = scripts.helpers.get_account(0)
    contract_user_account = scripts.helpers.get_account(1)
    
    lottery_contract = getLotteryContract(deploy_account)
    fee_required = lottery_contract.getEntranceFee();
    
    assert(fee_required > Web3.toWei(0.03232, "ether"))
    assert(fee_required < Web3.toWei(0.03432, "ether"))
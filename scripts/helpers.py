from web3 import Web3
from brownie import network, accounts, config, MockV3Aggregator, VRFCoordinatorV2Mock, LinkTokenMock, Contract

LOCAL_BLOCKCHAIN_ENVIRONMENTS = ["development", "easy-trail"]
FORKED_LOCAL_BLOCKCHAINS = ["mainnet-fork"]

def get_account(index):
    if network.show_active() in LOCAL_BLOCKCHAIN_ENVIRONMENTS or \
       network.show_active() in FORKED_LOCAL_BLOCKCHAINS:
        return accounts[index]
    else:
        return accounts.add(config["wallets"]["private_key_" + str(index)])

contract_name_to_type = \
{
    "eth-usd-price-feed" : MockV3Aggregator,
    "vrf-coordinator" : VRFCoordinatorV2Mock,
    "link_token_contract" : LinkTokenMock
}

DECIMALS = 18
INITIAL_VALUE = Web3.toWei(2000, "ether")
BASE_FEE = 100000000000000000  # The premium
GAS_PRICE_LINK = 1e9  # Some value calculated depending on the Layer 1 cost and Link

def deploy_mock(contract_name):
    deploy_account = get_account(0)
    print ("Deploying {}...".format(contract_name))
    contract_type = contract_name_to_type[contract_name]
    if( contract_name == "eth-usd-price-feed"):
        contract_type.deploy(DECIMALS, Web3.toWei(INITIAL_VALUE, "ether"), 
                                                       {"from":deploy_account})
    elif( contract_name == "vrf-coordinator"):
        contract_type.deploy(BASE_FEE, GAS_PRICE_LINK, {"from": deploy_account})

    elif( contract_name == "link_token_contract"):
        contract_type.deploy({"from": deploy_account});

    print ("{} deployed".format(contract_name))
    
def get_contract(contract_name):
    contract_type = contract_name_to_type[contract_name]
    if network.show_active() in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        if( len(contract_type) <= 0 ):
            deploy_mock(contract_name)
        contract = contract_type[-1]
    else:
        contract_address = config["networks"][network.show_active()][contract_name]
        contract = Contract.from_abi(contract_type._name, contract_address, contract_type.abi)
        
    return contract
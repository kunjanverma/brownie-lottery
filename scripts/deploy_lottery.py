import scripts.helpers
from brownie import Lottery, network, config

def deploy_lottery():
    deploy_account = scripts.helpers.get_account(0)
    lottery = Lottery.deploy(
        scripts.helpers.get_contract("eth-usd-price-feed").address,
        scripts.helpers.get_contract("vrf-coordinator").address,
        scripts.helpers.get_contract("link_token_contract").address,
        #9140,
        config["networks"][network.show_active()]["keyHash"],
        {"from" : deploy_account},
        publish_source = config["networks"][network.show_active()].get("verify",False)
        )
    
    print("Deployed Lottery!")

'''def deploy_ClientLib():
    deploy_account = scripts.helpers.get_account(0)
    clientLib = ClientLib.deploy({"from" : deploy_account},
                     publish_source = config["networks"][network.show_active()].get("verify",False));
'''
    
def check_and_deploy_lottery():
    deploy_account = scripts.helpers.get_account(0)
    if len(Lottery) <= 0:
        deploy_lottery()
    else:
        print("Lottery contract already exists, not deploying new contract")

    lottery = Lottery[-1]
    scripts.helpers.get_contract("link_token_contract").approve(lottery.address, 3000000000000000000,
                                                                {"from" : deploy_account})
    '''
    if len(ClientLib) <= 0:
        deploy_ClientLib()
    else:
        print("ClientLib contract already exists, not deploying new contract")
    '''

def fundSubscription():
    clientLib = ClientLib[-1]
    lottery = Lottery[-1]
    clientLib.topUpSubscription(scripts.helpers.get_contract("vrf-coordinator").address,
                                scripts.helpers.get_contract("link_token_contract").address,
                                lottery.s_subscriptionId,
                                1000000000000000000)
    
'''def check_and_fund_contract():
    deploy_account = scripts.helpers.get_account(0)
    lottery = Lottery[-1]
    if lottery.getBalance() < 3000000000000000000:
        remaining = lottery.getLinkAllowance(deploy_account,lottery.address) 
        if remaining < 3000000000000000000:
             lottery.approveLink(remaining+3000000000000000000, {"from":deploy_account});
   '''
    
def start_lottery():
    deploy_account = scripts.helpers.get_account(0)
    lottery = Lottery[-1]
    transaction=lottery.startLottery({"from":deploy_account})
    transaction.wait(1)
    print("Lottery started!!")

def enter_lottery(index):
    lottery_user_account = scripts.helpers.get_account(index)
    lottery = Lottery[-1]
    value_to_send = lottery.getEntranceFee() + 1000000
    transaction=lottery.buyTicket({"from":lottery_user_account, "value":value_to_send})
    transaction.wait(1)
    print("User entered lottery!!")
    
def end_lottery():
    deploy_account = scripts.helpers.get_account(0)
    lottery = Lottery[-1]
    transaction = lottery.endLottery({"from":deploy_account})
    transaction.wait(1)
    
    
def main():
    check_and_deploy_lottery()
    #check_and_fund_contract()
    #start_lottery()
    #enter_lottery(1)
    #enter_lottery(2)
    #end_lottery()